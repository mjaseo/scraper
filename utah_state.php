<?php
ini_set("display_errors",1);

if(file_exists('class.php')){
	require_once('class.php');
	$scraper = new Scraper;
}else{
	die('scraper file not found');
}

print "<pre>";
//do agree to set correct cookie
$scraper->postFields = 'agree=true';
$scraper->getContent('http://webapps.corrections.utah.gov/correctionsdynamic//Offender_agree');

$alpha = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
$results = array();

foreach($alpha as $a1){
	foreach($alpha as $a2){
		$url = 'http://webapps.corrections.utah.gov/correctionsdynamic/Offender_list';
		$scraper->postFields = 'firstName='.$a1.$a2.'&lastName=&middleName=';
		$page = $scraper->getContent($url,$scraper->cookieFile);
		
		if(preg_match_all('/<tr class="selectable-row" id="(\d+)">/',$page,$offenders,PREG_SET_ORDER)){
			
			foreach($offenders as $offender){
				$scraper->postFields = 'offenderNumber='.$offender[1];
				$detail = $scraper->getContent('http://webapps.corrections.utah.gov/correctionsdynamic/Offender_detail',$scraper->cookieFile);
				
				if(preg_match('/<li><b>Offender Name:(.*?)<\/li>/',$detail,$name)){ $results[$offender[1]]['name'] = $scraper->clean_string($name[1]); }
				if(preg_match('/<li><b>DOB:(.*?)<\/li>/',$detail,$dob)){ $results[$offender[1]]['DOB'] = $scraper->clean_string($dob[1]); }
				if(preg_match('/<li><b>Height:(.*)<\/li>/',$detail,$height)){ $results[$offender[1]]['height'] = $scraper->clean_string($height[1]); }
				if(preg_match('/<li><b>Weight:(.*?)<\/li>/',$detail,$weight)){ $results[$offender[1]]['weight'] = $scraper->clean_string($weight[1]); }
				if(preg_match('/<li><b>Sex:(.*?)<\/li>/',$detail,$sex)){ $results[$offender[1]]['sex'] = $scraper->clean_string($sex[1]); }
				
				if(preg_match_all('/<li>([A-Z\s]+)<\/li>/',$detail,$aliases,PREG_SET_ORDER)){
					foreach($aliases as $alias){
						$results[$offender[1]]['aliases'][] = $scraper->clean_string($alias[1]);
					}
				}
			}
		}
	}
}

print_r($results);

?>