<?php
ini_set("display_errors",1);

$domain = 'http://www.standard.net';

$next = true;
$p = $i = 0;
$results = array();

do{
	
	$page = getpage($domain.'/jail-mugs?page='.$p);
	
	if(preg_match_all('/class="views-field views-field-title">\s+<a href="(.*?)"><div>/i',$page,$inmates,PREG_SET_ORDER)){

		foreach($inmates as $inmate){
			$content = getpage($domain.$inmate[1]);
			
			if(preg_match('/<div class="views-field-field-mugshot-photo-fid">\s+<span class="field-content"><a href="(.*?)" title/',$content,$mugshot)){
				$results[$i]['mugshot'] = base64_encode(file_get_contents($mugshot[1]));
			}

			if(preg_match('/BOOKING DATE\:(.*?)CHARGES/',$content,$booking)){
				$results[$i]['booking_date'] = trim($booking[1]);
			}

			if(preg_match('/CHARGES:([^\00]*?)<hr \/>/',$content,$charges)){
				if(preg_match_all('/<li>(.*?)<\/li>/',$charges[1],$_charges,PREG_SET_ORDER)){
					foreach($_charges as $charge){
						$results[$i]['charges'][] = $charge[1];
					}
				}
			}

			if(preg_match('/<div class="panel-pane pane-page-title" >\s+<div class="pane-content">\s+<h1>(.*?)<\/h1>/',$content,$name)){
				$results[$i]['name'] = trim($name[1]);
			}
			$i++;
		}
		
	}
	if(preg_match('/<a href\="\/jail\-mugs\?page\=(\d+)" title\=\"Go to next page\"/',$page)){
		$p++;
	}else{ $next=false; }
	
}while($next);

print "<pre>";
print_r($results);

function getpage($url){
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    //curl_setopt($ch,CURLOPT_POST, TRUE);
    //curl_setopt($ch,CURLOPT_POSTFIELDS, array('filter' => '', 'start' => $start));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_REFERER, 'https://team.cachesheriff.com/ccso/inmate/index.php');
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
?>
