<?php header('Content-Type: text/html; charset=utf-8'); ini_set("display_errors",1);

function crawl($weburl,$post=null){
	$url = $weburl;
	$cookie = tempnam ("/tmp", "CURLCOOKIE");
		
	$ch = curl_init();
		
	curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
	curl_setopt( $ch, CURLOPT_URL, $url );
		
	curl_setopt( $ch, CURLOPT_COOKIESESSION, true);
	curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
	curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie );
		
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt( $ch, CURLOPT_ENCODING, "" );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
	
	$output = curl_exec($ch);
	return $output;
}

function dom($domHTML,$query){
	$dom = new DOMDocument();
	@$dom->loadHTML($domHTML);
			
	$xpath = new DomXPath($dom);
	return $xpath->query($query);
}

function thumbs($index){
	$alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
	return $alpha[$index];
}

function downLoadFile($folder,$url){

	     $dir = 'Collections/Apparel and Shoes 1';

	     if(!is_dir($dir)) mkdir($dir,0777);

	     $fp = fopen ($dir.'/'.$folder, 'w+');

     	 $ch = curl_init();
     
     	 curl_setopt($ch, CURLOPT_URL,$url);
     	 curl_setopt($ch, CURLOPT_HEADER, 0);
     	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     	 curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
          
     	 curl_setopt($ch, CURLOPT_FILE, $fp);
     	 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
     	 $content = curl_exec ($ch);
     	 fclose($fp);

     	 if($content === FALSE){
     	 	return curl_errno($ch);
     	 	die;
     	 }else{
     	 	return $content;
     	 }

     	curl_close($ch);
     }

$domain = 'https://www.smai.com.au';
$landing = crawl($domain . '/collections/apparel-shoes-1?page=3');
$grids = dom($landing, '//a[@class="grid__image"]');

if($grids->length > 0){

	foreach($grids as $grid){
		
		$pURL = $domain . $grid->getAttributeNode('href')->nodeValue;
		$page = crawl($pURL);
		
		if($page){
			
			$name = dom($page, '//h1[@class="heading"]');
			$thumbs = dom($page, '//ul[@id="ProductThumbs"]/li/a');
			$variants = dom($page, '//select[@class="product-single__variants"]/option');
			$img = dom($page, '//img[@id="ProductPhotoImg"]');
			$oImg = 'http:' . $img->item(0)->getAttributeNode('src')->nodeValue;
			
			$title = str_replace('/', '_', $name->item(0)->nodeValue);
			
			if($thumbs->length > 0){
				
				foreach($thumbs as $index => $thumb){
					
					$imgURL = 'http:' . $thumb->getAttributeNode('href')->nodeValue;
					downLoadFile(thumbs($index) . '_' . $title . '.jpg', $imgURL);
					print 'Added file: '.  thumbs($index) . '_' . $title . ".jpg \n";
				}
				
			}elseif($variants->length > 1){
				//print $title . ' => ' . $variants->length . '<br />';
				foreach($variants as $var){
				
					$varname = explode('-', $var->nodeValue);
					downLoadFile($title . '_' . str_replace('/', '_', trim($varname[0])) . '.jpg', $oImg);
					print 'Added file: ' . $title . '_' . trim($varname[0]) . ".jpg \n";
				
				}
				
			}else{
				downLoadFile($title . '.jpg', $oImg);
				print 'Added file:' . $title . ".jpg \n";
			}
			sleep(1);
		}
		
		//exit;
	}
}