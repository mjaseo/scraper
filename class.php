<?php

class Scraper{

	public $mainURL;
	public $postFields;
	public $cookieFile;
	
	function getContent($URL = null, $cookieFile = null, $delay=null, $header=null){
	
		$ch = curl_init();
		
		if(!is_null($URL)) $this->mainURL = $URL;
		       
		if(!empty($this->mainURL)) curl_setopt($ch, CURLOPT_URL,$this->mainURL);
		
		$this->cookieFile = (!is_null($cookieFile)) ? $cookieFile : tempnam ("/tmp", "CURLCOOKIE");
		
		if(!empty($this->postFields)){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postFields);
		}
		
		if(!empty($delay)){
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
		}
		
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile); 
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);  
		
		if(!is_null($header) && $header==1){     
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}else{
			curl_setopt($ch, CURLOPT_HEADER, 0);
		}

     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Ubuntu; X11; Linux i686; rv:8.0) Gecko/20100101 Firefox/8.0");	
     	curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
     	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE);
     	curl_setopt($ch, CURLOPT_AUTOREFERER,TRUE);

     	$content = curl_exec ($ch);

     	if($content === FALSE){
	     return curl_error($ch);
       	 die;
     	}else{
     	 return $content;
     	}
     	curl_close($ch);	
     }

     function downLoadFile($folder,$url){

	     $dir = 'dump/'.$folder;

	     if(!is_dir($dir)) mkdir($dir,0777);

	     $fp = fopen ($dir.'/'.$folder.'.pdf', 'w+');

     	 $ch = curl_init();
     
     	 curl_setopt($ch, CURLOPT_URL,$url);
     	 curl_setopt($ch, CURLOPT_HEADER, 0);
     	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     	 curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
          
     	 curl_setopt($ch, CURLOPT_FILE, $fp);
     	 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
     	 $content = curl_exec ($ch);
     	 fclose($fp);

     	 if($content === FALSE){
     	 	return curl_errno($ch);
     	 	die;
     	 }else{
     	 	return $content;
     	 }

     	curl_close($ch);
     }

     ##  Parse content
     function doParsing($item, $pattern){
     	if(preg_match_all($pattern, $item, $results))
     		return ($results);
     	else return false;
	    }
   
	    function clean_string($string,$allow=null){
		    return trim(strip_tags(preg_replace('/\s+/',' ',$string),"{$allow}"));
		}

	}

?>